﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using MySql.Data;
using MySql.Data.MySqlClient;

using Microsoft.Extensions.Configuration;

// todo : pasting affiliations
// todo : tags for add book
// todo : search
// todo : improve add book: affiliations? (for .edu)


namespace LibraryPDF
{
    public partial class Form1 : Form
    {
        private IConfiguration SqlConfig;
        MySqlConnection libraryConnection;
        string appPath;

        string bookRepositoryPath;
        string pubRepositoryPath;
        
        string pdfBookFilePath;
        string pdfPubFilePath;
        
        string bookTableName;
        string pubTableName;

        public Form1()
        {
            InitializeComponent();
            SqlConfig = new ConfigurationBuilder()
                .AddJsonFile("SqlConfig.json", optional: false, reloadOnChange: false)
                .Build();


            var userName = SqlConfig.GetSection("Credentials").GetSection("username").Value;
            var password = SqlConfig.GetSection("Credentials").GetSection("password").Value;
            var connectionString = String.Format("server=localhost;port=3306;userid={0};database=library_pdf;password={1};CertificateFile=C:\\Users\\tbodi\\OneDrive\\Documents\\Code\\MariaDB\\SSL_Certs\\ca.pem;SslMode=Required;",
                    userName,
                    password
                );

            libraryConnection = new MySqlConnection(connectionString);
            libraryConnection.Open();

            for (int i = 0; i <= listView1.Columns.Count - 1; ++i)
            {
                listView1.Columns[i].Width = -2;
            }

            for (int i = 0; i < listView2.Columns.Count - 1; ++i)
            {
                listView2.Columns[i].Width = -2;
            }

            bookTableName = "books_tb";
            pubTableName = "publications_tb";

            // used for tabView and Form
            pdfBookFilePath = "";
            pdfPubFilePath = "";

            appPath = AppDomain.CurrentDomain.BaseDirectory;

            if (!System.IO.Directory.Exists(appPath + "Books\\"))
            {
                throw new System.ApplicationException("There is a missing directory: " + "\\Books -> " + appPath);
            }
            else
            {
                bookRepositoryPath = appPath + "Books\\";
            }

            if (!System.IO.Directory.Exists(appPath + "Pubs\\"))
            {
                throw new System.ApplicationException("There is a missing directory: " + "\\Pubs -> " + appPath);
            }
            else
            {
                pubRepositoryPath = appPath + "Pubs\\";
            }

            VerifySecurity();
        }

        private void VerifySecurity()
        {
            string result = "";

            var queryString = "show session status like 'ssl_cipher';";

            var cmd = new MySqlCommand(queryString, libraryConnection);
            var reader = cmd.ExecuteReader();
            reader.Read();
            result = reader.GetString(1);
            reader.Close();
            richTextBox1.AppendText("Inserted into db.\r\n");

            richTextBox1.AppendText("Tls: " + result);
        }

        private bool IsValidDateString(string toCheck)
        {
            foreach (char c in toCheck)
            {
                if (c == ' ')
                    return false;
            }
            DateTime dt;

            try
            {
                dt = DateTime.Parse(toCheck);
            }
            catch (FormatException)
            {
                // highlight bad date
                return false;
            }

            return true;
        }

        private bool ParametersAreValid(string authorName, string ISBN, string ISSN, string Name)
        {
            if (ISBN.Length < 10 && ISBN.Length != 0)
                return false;

            if (ISSN.Length != 0 && ISSN.Length < 8)
                return false;

            if (Name.Length == 0)
                return false;
            return true;
        }

        private bool FileIsValid()
        {
            if (pdfBookFilePath == "")
            {
                return false;
            }
            //var fileInfo = new System.IO.FileInfo(pdfBookFilePath);
            return true;
        }

        private bool FileIsValid2()
        {
            if (pdfPubFilePath == "")
            {
                return false;
            }
            return true;
        }

        private bool AlreadyExistsSerial(string ISBN, string ISSN)
        {
            string queryString = "SELECT id FROM " + bookTableName + " where "; // ISBN=\"" + ISBN + "\" or ISSN=\"" + ISSN + "\"";

            if (ISBN.Length != 0)
            {
                queryString += "ISBN =\"" + ISBN + "\"";
                if (ISSN.Length != 0)
                {
                    queryString += " or ISSN=\"" + ISSN + "\"";
                }

            }
            else
            {
                queryString += "ISSN=\"" + ISSN + "\"";
            }

            var cmd = new MySqlCommand(queryString, libraryConnection);
            var reader = cmd.ExecuteReader();
            
            if (reader.Read())
            {
                reader.Close();
                return true;
            }

            reader.Close();
            return false;
        }

        private bool AlreadyExistsDOI(string DOI)
        {
            string queryString = String.Format("SELECT id FROM " + pubTableName + " where DOI=\"{0}\"", DOI);
            var cmd = new MySqlCommand(queryString, libraryConnection);
            var reader = cmd.ExecuteReader();
            
            if (reader.Read())
            {
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }

        private void ClearAllFields()
        {
            textBoxISBN.Text = "";
            textBoxISSN.Text = "";
            textBoxAuthor.Text = "";
            textBoxName.Text = "";
            textBoxYear.Text = "";
            textBoxFile.Text = "";
            
            pdfBookFilePath = "";
        }

        private void ClearAllFieldsPub()
        {
            pdfPubFilePath = "";
            
            textBoxDOI.Text = "";
            textBoxPubName.Text = "";
            textBoxPubPath.Text = "";
            textBoxAffiliationsPub.Text = "";
            textBoxYearPub.Text = "";
            textBoxTagsPub.Text = "";
        }

        private bool storeBook()
        {
            string destinationPath = bookRepositoryPath + new System.IO.FileInfo(pdfBookFilePath).Name;

            if (!System.IO.File.Exists(pdfBookFilePath) || System.IO.File.Exists(destinationPath))
            {
                //throw new System.ApplicationException("File does not exist.");
                return false;
            }

            /*
            if (new System.IO.FileInfo(destinationPath).Length == new System.IO.FileInfo(pdfBookFilePath).Length && 
                System.IO.File.ReadAllBytes(destinationPath).SequenceEqual(System.IO.File.ReadAllBytes(pdfBookFilePath)))
            {
                // same file
                return false;
            }*/

            System.IO.File.Copy(pdfBookFilePath, destinationPath);

            return true;
        }

        private bool storePublication()
        {
            string destPath = pubRepositoryPath + new System.IO.FileInfo(pdfPubFilePath).Name;

            if (!System.IO.File.Exists(pdfPubFilePath) || System.IO.File.Exists(destPath))
                return false;

            System.IO.File.Copy(pdfPubFilePath, destPath); // todo try catch
            return true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            #region check_textboxes
            if (textBoxISBN.Text == "" && textBoxISSN.Text == "")
            {
                richTextBox1.AppendText("ERROR: Both ISBN and ISSN can't be empty. Please fill at least one. \r\n");
                labelISBN.ForeColor = Color.Red;
                labelISSN.ForeColor = Color.Red;
                return;
            }
            else
            {
                labelISBN.ForeColor = Color.Black;
                labelISSN.ForeColor = Color.Black;
            }

            if (textBoxName.Text == "")
            {
                labelName.ForeColor = Color.Red;
                richTextBox1.AppendText("ERROR: Name can't be empty.\r\n");
                return;
            }
            else
            {
                labelName.ForeColor = Color.Black;
            }
            #endregion

            #region validate
            string year = textBoxYear.Text;

            if (!IsValidDateString(year))
            {
                // highlight
                labelYear.ForeColor = Color.Red;
                return;
            }
            else
            {
                DateTime dt = DateTime.Parse(year);
                year = dt.ToString("yyyy-MM-dd");
                labelYear.ForeColor = Color.Black;
            }

            string authorName = textBoxAuthor.Text;
            string ISBN = textBoxISBN.Text;
            string ISSN = textBoxISSN.Text;
            string Name = textBoxName.Text;

            if (!ParametersAreValid(authorName, ISBN, ISSN, Name))
            {
                richTextBox1.AppendText("Please check parameters. There were problems with the file format.\r\n");
                return;
            }

            if (!FileIsValid())
            {
                richTextBox1.AppendText("File is not valid. Please re-select a file.\r\n");
                return;
            }

            if (AlreadyExistsSerial(ISBN, ISSN))
            {
                richTextBox1.AppendText("An object with this ISBN or ISSN already exists.\r\n");
                return;
            }
            #endregion

            #region database_insert
            string queryString = String.Format("INSERT INTO " + bookTableName + " SET ISBN=\"{0}\", ISSN=\"{1}\", Name=\"{2}\", Year=\"{3}\", Author=\"{4}\"",
                ISBN,
                ISSN,
                Name,
                year,
                authorName
                );

            System.Console.WriteLine(queryString);

            if (libraryConnection.State == ConnectionState.Open)
            {
                if (!storeBook())
                {
                    // todo try catch message
                    richTextBox1.AppendText("Storing file failed. Please try again.\r\n");
                    return;
                }

                string dateAdded = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                queryString += String.Format(", Date_Added=\"{0}\"", dateAdded);

                string fileRef = bookRepositoryPath.Replace(@"\",@"\\") + new System.IO.FileInfo(pdfBookFilePath).Name;
                queryString += ", FS_Ref=\"" + fileRef + "\""; 

                var cmd = new MySqlCommand(queryString, libraryConnection);
                var reader = cmd.ExecuteReader();
                reader.Read();
                reader.Close();
                richTextBox1.AppendText("Inserted into db.\r\n");
                ClearAllFields();
            }
            else
            {
                richTextBox1.AppendText("DB connection failed. Please restart app.\r\n");
            }
            #endregion

        }

        private void buttonChooseFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            openFileDialog1.InitialDirectory = "C:\\";
            openFileDialog1.Filter = "PDF Files (.pdf)|*.pdf";
            openFileDialog1.FilterIndex = 0;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                string selectedFileName = openFileDialog1.FileName;
                long length = new System.IO.FileInfo(selectedFileName).Length;
                if (length > 256000000)
                    return;

                textBoxFile.Text = selectedFileName;
                pdfBookFilePath = selectedFileName;

                textBoxName.Text = new System.IO.FileInfo(pdfBookFilePath).Name;

                richTextBox1.AppendText(String.Format("Selected file named \"{1}\"with size {0} bytes\r\n", length, new System.IO.FileInfo(selectedFileName).Name));
            }

        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (libraryConnection.State == ConnectionState.Open)
                libraryConnection.Close();
        }

        private void tabControl1_Selected(object sender, TabControlEventArgs e)
        {
            if (e.TabPageIndex == 2)
            {
                string queryString = "SELECT id,ISBN,ISSN,Name,Year,Author,FS_Ref,Date_Added FROM " + bookTableName + "";
                var cmd = new MySqlCommand(queryString, libraryConnection);
                var reader = cmd.ExecuteReader();
                listView1.Items.Clear();
                while (reader.Read())
                {
                    string id = reader.GetString(0);
                    string ISBN = reader.GetString(1);
                    string ISSN = reader.GetString(2);
                    string Name = reader.GetString(3);
                    string Year = reader.GetString(4).Split(' ')[0];
                    string Author = reader.GetString(5);
                    string FS_Ref = reader.GetString(6);
                    string Date_Added = reader.GetString(7);
                    //string[] row = { id, ISBN, ISSN, Name, Year, Author };
                    var listViewItem = new ListViewItem(id);
                    listViewItem.SubItems.Add(ISBN);
                    listViewItem.SubItems.Add(ISSN);
                    listViewItem.SubItems.Add(Name);
                    listViewItem.SubItems.Add(Year);
                    listViewItem.SubItems.Add(Author);
                    listViewItem.SubItems.Add(FS_Ref);
                    listViewItem.SubItems.Add(Date_Added);
                    //System.Console.WriteLine("Added.");
                    listView1.Items.Add(listViewItem);

                    listView1.Update();
                }
                reader.Close();
                //System.Console.WriteLine("Path 1");
            }
            else if (e.TabPageIndex == 3)
            {
                string queryString = "SELECT id,doi,Name,Year,Affiliations,FS_Ref,Tags,Date_Added FROM " + pubTableName + "";
                var cmd = new MySqlCommand(queryString, libraryConnection);
                var reader = cmd.ExecuteReader();
                listView2.Items.Clear();

                while (reader.Read())
                {
                    string id = reader.GetString(0);
                    string doi = reader.GetString(1);
                    string Name = reader.GetString(2);
                    string Year = reader.GetString(3);
                    string Affiliations = reader.GetString(4);
                    string FS_Ref = reader.GetString(5);
                    string Tags = reader.GetString(6);
                    string Date_Added = reader.GetString(7);

                    var listViewItem = new ListViewItem(id);
                    listViewItem.SubItems.Add(doi);
                    listViewItem.SubItems.Add(Name);
                    listViewItem.SubItems.Add(Year);
                    listViewItem.SubItems.Add(Affiliations);
                    listViewItem.SubItems.Add(FS_Ref);
                    listViewItem.SubItems.Add(Tags);
                    listViewItem.SubItems.Add(Date_Added);

                    listView2.Items.Add(listViewItem);
                    listView2.Update();
                }
                reader.Close();

                //System.Console.WriteLine("Path 2");
            }
        }

        private void buttonClearFields_Click(object sender, EventArgs e)
        {
            ClearAllFields();
        }

        private void buttonBrowse_Click(object sender, EventArgs e)
        {
            // used by Publication tab
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            openFileDialog1.InitialDirectory = "C:\\";
            openFileDialog1.Filter = "PDF Files (.pdf)|*.pdf";
            openFileDialog1.FilterIndex = 0;
            openFileDialog1.RestoreDirectory = true;

            // using file dialog
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                string selectedFileName = openFileDialog1.FileName;
                long length = new System.IO.FileInfo(selectedFileName).Length;

                // update form members
                textBoxPubPath.Text = selectedFileName;
                textBoxPubName.Text = new System.IO.FileInfo(selectedFileName).Name;
                pdfPubFilePath = selectedFileName;

                // signal richText
                richTextBoxPub.AppendText(String.Format("Selected file named \"{1}\"with size {0} bytes\r\n", length, new System.IO.FileInfo(selectedFileName).Name));
            }
        }

        private void buttonClearPub_Click(object sender, EventArgs e)
        {
            ClearAllFieldsPub();
            richTextBoxPub.AppendText("Cleared all fields.\r\n");
        }

        private void buttonSubmitPub_Click(object sender, EventArgs e)
        {
            // 1. Check field formatting
            #region check_textboxes
            if (textBoxDOI.Text == "")
            {
                labelDOI.ForeColor = Color.Red;
                richTextBoxPub.AppendText("ERROR: DOI can't be empty. Please fill the information correctly.\r\n");
                return;
            }
            else
            {
                labelDOI.ForeColor = Color.Black;
            }

            if (textBoxPubName.Text == "")
            {
                labelPubName.ForeColor = Color.Red;
                richTextBoxPub.AppendText("ERROR: Name can't be empty. Please fill the information correctly.\r\n");
                return;
            }
            else
            {
                labelPubName.ForeColor = Color.Black;
            }
            #endregion

            #region validate
            string year = textBoxYearPub.Text;

            if (!IsValidDateString(year))
            {
                // highlight
                labelYearPub.ForeColor = Color.Red;
                return;
            }
            else
            {
                DateTime dt = DateTime.Parse(year);
                year = dt.ToString("yyyy-MM-dd");
                labelYearPub.ForeColor = Color.Black;
            }

            string DOI = textBoxDOI.Text;
            string Name = textBoxPubName.Text;
            string Affiliations = textBoxAffiliationsPub.Text;
            string FS_Ref = textBoxPubPath.Text;
            string Tags = textBoxTagsPub.Text;

            if (!FileIsValid2())
            {
                richTextBoxPub.AppendText("File is not valid. Please re-select a file.\r\n");
                return;
            }

            if (AlreadyExistsDOI(DOI))
            {
                richTextBoxPub.AppendText("DOI already exists.\r\n");
                return;
            }

            #endregion

            #region database_insert

            string fileRef = pubRepositoryPath.Replace(@"\", @"\\") + new System.IO.FileInfo(pdfPubFilePath).Name;
            string dateAdded = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

            string queryString = String.Format("INSERT INTO " + pubTableName + " SET doi=\"{0}\", Name=\"{1}\", Year=\"{2}\", Affiliations=\"{3}\", FS_Ref=\"{4}\", Tags=\"{5}\", Date_Added=\"{6}\"",
                DOI,
                Name,
                year,
                Affiliations,
                fileRef,
                Tags,
                dateAdded
                );

            System.Console.WriteLine(queryString);

            if (libraryConnection.State == ConnectionState.Open)
            {
                if (!storePublication())
                {
                    richTextBoxPub.AppendText("Storing publication failed. \r\n");
                    return;
                }

                var cmd = new MySqlCommand(queryString, libraryConnection);
                var reader = cmd.ExecuteReader();
                reader.Read();
                reader.Close();
                richTextBox1.AppendText("Inserted into db.\r\n");
                ClearAllFieldsPub();
            }
            else
            {
                richTextBox1.AppendText("DB connection failed. Please restart app.\r\n");
            }

            #endregion
        }
    }
}
