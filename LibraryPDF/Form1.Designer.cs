﻿namespace LibraryPDF
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPageAddBook = new System.Windows.Forms.TabPage();
            this.labelTags = new System.Windows.Forms.Label();
            this.textBoxTags = new System.Windows.Forms.TextBox();
            this.buttonClearFields = new System.Windows.Forms.Button();
            this.buttonChooseFile = new System.Windows.Forms.Button();
            this.textBoxFile = new System.Windows.Forms.TextBox();
            this.labelFile = new System.Windows.Forms.Label();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.submitButton = new System.Windows.Forms.Button();
            this.labelAuthor = new System.Windows.Forms.Label();
            this.labelYear = new System.Windows.Forms.Label();
            this.labelName = new System.Windows.Forms.Label();
            this.labelISSN = new System.Windows.Forms.Label();
            this.labelISBN = new System.Windows.Forms.Label();
            this.textBoxAuthor = new System.Windows.Forms.TextBox();
            this.textBoxYear = new System.Windows.Forms.TextBox();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.textBoxISSN = new System.Windows.Forms.TextBox();
            this.textBoxISBN = new System.Windows.Forms.TextBox();
            this.tabPageAddPub = new System.Windows.Forms.TabPage();
            this.textBoxTagsPub = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxAffiliationsPub = new System.Windows.Forms.TextBox();
            this.labelAffiliationsPub = new System.Windows.Forms.Label();
            this.textBoxYearPub = new System.Windows.Forms.TextBox();
            this.labelYearPub = new System.Windows.Forms.Label();
            this.richTextBoxPub = new System.Windows.Forms.RichTextBox();
            this.buttonClearPub = new System.Windows.Forms.Button();
            this.buttonSubmitPub = new System.Windows.Forms.Button();
            this.buttonBrowse = new System.Windows.Forms.Button();
            this.textBoxPubPath = new System.Windows.Forms.TextBox();
            this.textBoxPubName = new System.Windows.Forms.TextBox();
            this.labelPubPath = new System.Windows.Forms.Label();
            this.labelPubName = new System.Windows.Forms.Label();
            this.labelDOI = new System.Windows.Forms.Label();
            this.textBoxDOI = new System.Windows.Forms.TextBox();
            this.tabPageViewBooks = new System.Windows.Forms.TabPage();
            this.listView1 = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tabPageViewPubs = new System.Windows.Forms.TabPage();
            this.listView2 = new System.Windows.Forms.ListView();
            this.columnHeader_idpub = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader_doi = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader_name = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader_year = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader_affiliations = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader_fs_ref = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader_Tags = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.customInstaller1 = new MySql.Data.MySqlClient.CustomInstaller();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader_time_added = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tabControl1.SuspendLayout();
            this.tabPageAddBook.SuspendLayout();
            this.tabPageAddPub.SuspendLayout();
            this.tabPageViewBooks.SuspendLayout();
            this.tabPageViewPubs.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPageAddBook);
            this.tabControl1.Controls.Add(this.tabPageAddPub);
            this.tabControl1.Controls.Add(this.tabPageViewBooks);
            this.tabControl1.Controls.Add(this.tabPageViewPubs);
            this.tabControl1.Cursor = System.Windows.Forms.Cursors.Default;
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1146, 450);
            this.tabControl1.TabIndex = 0;
            this.tabControl1.Selected += new System.Windows.Forms.TabControlEventHandler(this.tabControl1_Selected);
            // 
            // tabPageAddBook
            // 
            this.tabPageAddBook.Controls.Add(this.labelTags);
            this.tabPageAddBook.Controls.Add(this.textBoxTags);
            this.tabPageAddBook.Controls.Add(this.buttonClearFields);
            this.tabPageAddBook.Controls.Add(this.buttonChooseFile);
            this.tabPageAddBook.Controls.Add(this.textBoxFile);
            this.tabPageAddBook.Controls.Add(this.labelFile);
            this.tabPageAddBook.Controls.Add(this.richTextBox1);
            this.tabPageAddBook.Controls.Add(this.submitButton);
            this.tabPageAddBook.Controls.Add(this.labelAuthor);
            this.tabPageAddBook.Controls.Add(this.labelYear);
            this.tabPageAddBook.Controls.Add(this.labelName);
            this.tabPageAddBook.Controls.Add(this.labelISSN);
            this.tabPageAddBook.Controls.Add(this.labelISBN);
            this.tabPageAddBook.Controls.Add(this.textBoxAuthor);
            this.tabPageAddBook.Controls.Add(this.textBoxYear);
            this.tabPageAddBook.Controls.Add(this.textBoxName);
            this.tabPageAddBook.Controls.Add(this.textBoxISSN);
            this.tabPageAddBook.Controls.Add(this.textBoxISBN);
            this.tabPageAddBook.Location = new System.Drawing.Point(4, 22);
            this.tabPageAddBook.Name = "tabPageAddBook";
            this.tabPageAddBook.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageAddBook.Size = new System.Drawing.Size(1114, 400);
            this.tabPageAddBook.TabIndex = 0;
            this.tabPageAddBook.Text = "Add Book";
            this.tabPageAddBook.UseVisualStyleBackColor = true;
            // 
            // labelTags
            // 
            this.labelTags.AutoSize = true;
            this.labelTags.Location = new System.Drawing.Point(410, 55);
            this.labelTags.Name = "labelTags";
            this.labelTags.Size = new System.Drawing.Size(34, 13);
            this.labelTags.TabIndex = 18;
            this.labelTags.Text = "Tags:";
            // 
            // textBoxTags
            // 
            this.textBoxTags.Location = new System.Drawing.Point(410, 74);
            this.textBoxTags.Multiline = true;
            this.textBoxTags.Name = "textBoxTags";
            this.textBoxTags.Size = new System.Drawing.Size(227, 156);
            this.textBoxTags.TabIndex = 17;
            // 
            // buttonClearFields
            // 
            this.buttonClearFields.Location = new System.Drawing.Point(229, 297);
            this.buttonClearFields.Name = "buttonClearFields";
            this.buttonClearFields.Size = new System.Drawing.Size(63, 51);
            this.buttonClearFields.TabIndex = 16;
            this.buttonClearFields.Text = "Clear";
            this.buttonClearFields.UseVisualStyleBackColor = true;
            this.buttonClearFields.Click += new System.EventHandler(this.buttonClearFields_Click);
            // 
            // buttonChooseFile
            // 
            this.buttonChooseFile.Location = new System.Drawing.Point(319, 241);
            this.buttonChooseFile.Name = "buttonChooseFile";
            this.buttonChooseFile.Size = new System.Drawing.Size(54, 21);
            this.buttonChooseFile.TabIndex = 15;
            this.buttonChooseFile.Text = "Browse";
            this.buttonChooseFile.UseVisualStyleBackColor = true;
            this.buttonChooseFile.Click += new System.EventHandler(this.buttonChooseFile_Click);
            // 
            // textBoxFile
            // 
            this.textBoxFile.Location = new System.Drawing.Point(108, 241);
            this.textBoxFile.Name = "textBoxFile";
            this.textBoxFile.ReadOnly = true;
            this.textBoxFile.Size = new System.Drawing.Size(205, 20);
            this.textBoxFile.TabIndex = 14;
            // 
            // labelFile
            // 
            this.labelFile.AutoSize = true;
            this.labelFile.Location = new System.Drawing.Point(19, 249);
            this.labelFile.Name = "labelFile";
            this.labelFile.Size = new System.Drawing.Size(26, 13);
            this.labelFile.TabIndex = 13;
            this.labelFile.Text = "File:";
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(710, 9);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(360, 339);
            this.richTextBox1.TabIndex = 12;
            this.richTextBox1.Text = "";
            // 
            // submitButton
            // 
            this.submitButton.Location = new System.Drawing.Point(22, 297);
            this.submitButton.Name = "submitButton";
            this.submitButton.Size = new System.Drawing.Size(161, 51);
            this.submitButton.TabIndex = 10;
            this.submitButton.Text = "Submit";
            this.submitButton.UseVisualStyleBackColor = true;
            this.submitButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // labelAuthor
            // 
            this.labelAuthor.AutoSize = true;
            this.labelAuthor.Location = new System.Drawing.Point(16, 217);
            this.labelAuthor.Name = "labelAuthor";
            this.labelAuthor.Size = new System.Drawing.Size(41, 13);
            this.labelAuthor.TabIndex = 9;
            this.labelAuthor.Text = "Author:";
            // 
            // labelYear
            // 
            this.labelYear.AutoSize = true;
            this.labelYear.Location = new System.Drawing.Point(16, 170);
            this.labelYear.Name = "labelYear";
            this.labelYear.Size = new System.Drawing.Size(32, 13);
            this.labelYear.TabIndex = 8;
            this.labelYear.Text = "Year:";
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Location = new System.Drawing.Point(16, 131);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(38, 13);
            this.labelName.TabIndex = 7;
            this.labelName.Text = "Name:";
            // 
            // labelISSN
            // 
            this.labelISSN.AutoSize = true;
            this.labelISSN.Location = new System.Drawing.Point(16, 74);
            this.labelISSN.Name = "labelISSN";
            this.labelISSN.Size = new System.Drawing.Size(35, 13);
            this.labelISSN.TabIndex = 6;
            this.labelISSN.Text = "ISSN:";
            // 
            // labelISBN
            // 
            this.labelISBN.AutoSize = true;
            this.labelISBN.Location = new System.Drawing.Point(16, 28);
            this.labelISBN.Name = "labelISBN";
            this.labelISBN.Size = new System.Drawing.Size(35, 13);
            this.labelISBN.TabIndex = 5;
            this.labelISBN.Text = "ISBN:";
            // 
            // textBoxAuthor
            // 
            this.textBoxAuthor.Location = new System.Drawing.Point(108, 210);
            this.textBoxAuthor.Name = "textBoxAuthor";
            this.textBoxAuthor.Size = new System.Drawing.Size(205, 20);
            this.textBoxAuthor.TabIndex = 4;
            // 
            // textBoxYear
            // 
            this.textBoxYear.Location = new System.Drawing.Point(108, 167);
            this.textBoxYear.Name = "textBoxYear";
            this.textBoxYear.Size = new System.Drawing.Size(205, 20);
            this.textBoxYear.TabIndex = 3;
            // 
            // textBoxName
            // 
            this.textBoxName.Location = new System.Drawing.Point(108, 124);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(205, 20);
            this.textBoxName.TabIndex = 2;
            // 
            // textBoxISSN
            // 
            this.textBoxISSN.Location = new System.Drawing.Point(108, 71);
            this.textBoxISSN.Name = "textBoxISSN";
            this.textBoxISSN.Size = new System.Drawing.Size(205, 20);
            this.textBoxISSN.TabIndex = 1;
            // 
            // textBoxISBN
            // 
            this.textBoxISBN.Location = new System.Drawing.Point(108, 28);
            this.textBoxISBN.Name = "textBoxISBN";
            this.textBoxISBN.Size = new System.Drawing.Size(205, 20);
            this.textBoxISBN.TabIndex = 0;
            // 
            // tabPageAddPub
            // 
            this.tabPageAddPub.Controls.Add(this.textBoxTagsPub);
            this.tabPageAddPub.Controls.Add(this.label1);
            this.tabPageAddPub.Controls.Add(this.textBoxAffiliationsPub);
            this.tabPageAddPub.Controls.Add(this.labelAffiliationsPub);
            this.tabPageAddPub.Controls.Add(this.textBoxYearPub);
            this.tabPageAddPub.Controls.Add(this.labelYearPub);
            this.tabPageAddPub.Controls.Add(this.richTextBoxPub);
            this.tabPageAddPub.Controls.Add(this.buttonClearPub);
            this.tabPageAddPub.Controls.Add(this.buttonSubmitPub);
            this.tabPageAddPub.Controls.Add(this.buttonBrowse);
            this.tabPageAddPub.Controls.Add(this.textBoxPubPath);
            this.tabPageAddPub.Controls.Add(this.textBoxPubName);
            this.tabPageAddPub.Controls.Add(this.labelPubPath);
            this.tabPageAddPub.Controls.Add(this.labelPubName);
            this.tabPageAddPub.Controls.Add(this.labelDOI);
            this.tabPageAddPub.Controls.Add(this.textBoxDOI);
            this.tabPageAddPub.Location = new System.Drawing.Point(4, 22);
            this.tabPageAddPub.Name = "tabPageAddPub";
            this.tabPageAddPub.Size = new System.Drawing.Size(1114, 400);
            this.tabPageAddPub.TabIndex = 2;
            this.tabPageAddPub.Text = "Add Publication";
            this.tabPageAddPub.UseVisualStyleBackColor = true;
            // 
            // textBoxTagsPub
            // 
            this.textBoxTagsPub.Location = new System.Drawing.Point(136, 249);
            this.textBoxTagsPub.Name = "textBoxTagsPub";
            this.textBoxTagsPub.Size = new System.Drawing.Size(234, 20);
            this.textBoxTagsPub.TabIndex = 15;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(64, 252);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 14;
            this.label1.Text = "Tags:";
            // 
            // textBoxAffiliationsPub
            // 
            this.textBoxAffiliationsPub.Location = new System.Drawing.Point(136, 202);
            this.textBoxAffiliationsPub.Name = "textBoxAffiliationsPub";
            this.textBoxAffiliationsPub.Size = new System.Drawing.Size(234, 20);
            this.textBoxAffiliationsPub.TabIndex = 13;
            // 
            // labelAffiliationsPub
            // 
            this.labelAffiliationsPub.AutoSize = true;
            this.labelAffiliationsPub.Location = new System.Drawing.Point(41, 205);
            this.labelAffiliationsPub.Name = "labelAffiliationsPub";
            this.labelAffiliationsPub.Size = new System.Drawing.Size(57, 13);
            this.labelAffiliationsPub.TabIndex = 12;
            this.labelAffiliationsPub.Text = "Affiliations:";
            // 
            // textBoxYearPub
            // 
            this.textBoxYearPub.Location = new System.Drawing.Point(136, 163);
            this.textBoxYearPub.Name = "textBoxYearPub";
            this.textBoxYearPub.Size = new System.Drawing.Size(234, 20);
            this.textBoxYearPub.TabIndex = 11;
            // 
            // labelYearPub
            // 
            this.labelYearPub.AutoSize = true;
            this.labelYearPub.Location = new System.Drawing.Point(60, 163);
            this.labelYearPub.Name = "labelYearPub";
            this.labelYearPub.Size = new System.Drawing.Size(32, 13);
            this.labelYearPub.TabIndex = 10;
            this.labelYearPub.Text = "Year:";
            // 
            // richTextBoxPub
            // 
            this.richTextBoxPub.Location = new System.Drawing.Point(778, 32);
            this.richTextBoxPub.Name = "richTextBoxPub";
            this.richTextBoxPub.Size = new System.Drawing.Size(319, 349);
            this.richTextBoxPub.TabIndex = 9;
            this.richTextBoxPub.Text = "";
            // 
            // buttonClearPub
            // 
            this.buttonClearPub.Location = new System.Drawing.Point(273, 288);
            this.buttonClearPub.Name = "buttonClearPub";
            this.buttonClearPub.Size = new System.Drawing.Size(75, 46);
            this.buttonClearPub.TabIndex = 8;
            this.buttonClearPub.Text = "Clear";
            this.buttonClearPub.UseVisualStyleBackColor = true;
            this.buttonClearPub.Click += new System.EventHandler(this.buttonClearPub_Click);
            // 
            // buttonSubmitPub
            // 
            this.buttonSubmitPub.Location = new System.Drawing.Point(136, 288);
            this.buttonSubmitPub.Name = "buttonSubmitPub";
            this.buttonSubmitPub.Size = new System.Drawing.Size(85, 46);
            this.buttonSubmitPub.TabIndex = 7;
            this.buttonSubmitPub.Text = "Submit";
            this.buttonSubmitPub.UseVisualStyleBackColor = true;
            this.buttonSubmitPub.Click += new System.EventHandler(this.buttonSubmitPub_Click);
            // 
            // buttonBrowse
            // 
            this.buttonBrowse.Location = new System.Drawing.Point(377, 108);
            this.buttonBrowse.Name = "buttonBrowse";
            this.buttonBrowse.Size = new System.Drawing.Size(65, 20);
            this.buttonBrowse.TabIndex = 6;
            this.buttonBrowse.Text = "Browse";
            this.buttonBrowse.UseVisualStyleBackColor = true;
            this.buttonBrowse.Click += new System.EventHandler(this.buttonBrowse_Click);
            // 
            // textBoxPubPath
            // 
            this.textBoxPubPath.Location = new System.Drawing.Point(136, 108);
            this.textBoxPubPath.Name = "textBoxPubPath";
            this.textBoxPubPath.ReadOnly = true;
            this.textBoxPubPath.Size = new System.Drawing.Size(234, 20);
            this.textBoxPubPath.TabIndex = 5;
            // 
            // textBoxPubName
            // 
            this.textBoxPubName.Location = new System.Drawing.Point(136, 67);
            this.textBoxPubName.Name = "textBoxPubName";
            this.textBoxPubName.Size = new System.Drawing.Size(234, 20);
            this.textBoxPubName.TabIndex = 4;
            // 
            // labelPubPath
            // 
            this.labelPubPath.AutoSize = true;
            this.labelPubPath.Location = new System.Drawing.Point(60, 115);
            this.labelPubPath.Name = "labelPubPath";
            this.labelPubPath.Size = new System.Drawing.Size(32, 13);
            this.labelPubPath.TabIndex = 3;
            this.labelPubPath.Text = "Path:";
            // 
            // labelPubName
            // 
            this.labelPubName.AutoSize = true;
            this.labelPubName.Location = new System.Drawing.Point(60, 70);
            this.labelPubName.Name = "labelPubName";
            this.labelPubName.Size = new System.Drawing.Size(38, 13);
            this.labelPubName.TabIndex = 2;
            this.labelPubName.Text = "Name:";
            // 
            // labelDOI
            // 
            this.labelDOI.AutoSize = true;
            this.labelDOI.Location = new System.Drawing.Point(63, 32);
            this.labelDOI.Name = "labelDOI";
            this.labelDOI.Size = new System.Drawing.Size(29, 13);
            this.labelDOI.TabIndex = 1;
            this.labelDOI.Text = "DOI:";
            // 
            // textBoxDOI
            // 
            this.textBoxDOI.Location = new System.Drawing.Point(136, 29);
            this.textBoxDOI.Name = "textBoxDOI";
            this.textBoxDOI.Size = new System.Drawing.Size(234, 20);
            this.textBoxDOI.TabIndex = 0;
            // 
            // tabPageViewBooks
            // 
            this.tabPageViewBooks.Controls.Add(this.listView1);
            this.tabPageViewBooks.Location = new System.Drawing.Point(4, 22);
            this.tabPageViewBooks.Name = "tabPageViewBooks";
            this.tabPageViewBooks.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageViewBooks.Size = new System.Drawing.Size(1138, 424);
            this.tabPageViewBooks.TabIndex = 1;
            this.tabPageViewBooks.Text = "View Books";
            this.tabPageViewBooks.UseVisualStyleBackColor = true;
            // 
            // listView1
            // 
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader6,
            this.columnHeader7,
            this.columnHeader8});
            this.listView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView1.HideSelection = false;
            this.listView1.Location = new System.Drawing.Point(3, 3);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(1132, 418);
            this.listView1.TabIndex = 0;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "id";
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "ISBN";
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "ISSN";
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Name";
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Year";
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Author";
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "FS_Ref";
            // 
            // tabPageViewPubs
            // 
            this.tabPageViewPubs.Controls.Add(this.listView2);
            this.tabPageViewPubs.Location = new System.Drawing.Point(4, 22);
            this.tabPageViewPubs.Name = "tabPageViewPubs";
            this.tabPageViewPubs.Size = new System.Drawing.Size(1138, 424);
            this.tabPageViewPubs.TabIndex = 3;
            this.tabPageViewPubs.Text = "View Publications";
            this.tabPageViewPubs.UseVisualStyleBackColor = true;
            // 
            // listView2
            // 
            this.listView2.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader_idpub,
            this.columnHeader_doi,
            this.columnHeader_name,
            this.columnHeader_year,
            this.columnHeader_affiliations,
            this.columnHeader_fs_ref,
            this.columnHeader_Tags,
            this.columnHeader_time_added});
            this.listView2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView2.HideSelection = false;
            this.listView2.Location = new System.Drawing.Point(0, 0);
            this.listView2.Name = "listView2";
            this.listView2.Size = new System.Drawing.Size(1138, 424);
            this.listView2.TabIndex = 0;
            this.listView2.UseCompatibleStateImageBehavior = false;
            this.listView2.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader_idpub
            // 
            this.columnHeader_idpub.Text = "id";
            // 
            // columnHeader_doi
            // 
            this.columnHeader_doi.Text = "DOI";
            // 
            // columnHeader_name
            // 
            this.columnHeader_name.Text = "Name";
            // 
            // columnHeader_year
            // 
            this.columnHeader_year.Text = "Year";
            // 
            // columnHeader_affiliations
            // 
            this.columnHeader_affiliations.Text = "Affiliations";
            // 
            // columnHeader_fs_ref
            // 
            this.columnHeader_fs_ref.Text = "FS_Ref";
            // 
            // columnHeader_Tags
            // 
            this.columnHeader_Tags.Text = "Tags";
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "Time Added";
            // 
            // columnHeader_time_added
            // 
            this.columnHeader_time_added.Text = "Time Added";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1146, 450);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form1";
            this.Text = "Library";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.tabControl1.ResumeLayout(false);
            this.tabPageAddBook.ResumeLayout(false);
            this.tabPageAddBook.PerformLayout();
            this.tabPageAddPub.ResumeLayout(false);
            this.tabPageAddPub.PerformLayout();
            this.tabPageViewBooks.ResumeLayout(false);
            this.tabPageViewPubs.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPageAddBook;
        private System.Windows.Forms.TabPage tabPageViewBooks;
        private System.Windows.Forms.Button submitButton;
        private System.Windows.Forms.Label labelAuthor;
        private System.Windows.Forms.Label labelYear;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.Label labelISSN;
        private System.Windows.Forms.Label labelISBN;
        private System.Windows.Forms.TextBox textBoxAuthor;
        private System.Windows.Forms.TextBox textBoxYear;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.TextBox textBoxISSN;
        private System.Windows.Forms.TextBox textBoxISBN;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Button buttonChooseFile;
        private System.Windows.Forms.TextBox textBoxFile;
        private System.Windows.Forms.Label labelFile;
        private MySql.Data.MySqlClient.CustomInstaller customInstaller1;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.Button buttonClearFields;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Label labelTags;
        private System.Windows.Forms.TextBox textBoxTags;
        private System.Windows.Forms.TabPage tabPageAddPub;
        private System.Windows.Forms.Button buttonBrowse;
        private System.Windows.Forms.TextBox textBoxPubPath;
        private System.Windows.Forms.TextBox textBoxPubName;
        private System.Windows.Forms.Label labelPubPath;
        private System.Windows.Forms.Label labelPubName;
        private System.Windows.Forms.Label labelDOI;
        private System.Windows.Forms.TextBox textBoxDOI;
        private System.Windows.Forms.Button buttonClearPub;
        private System.Windows.Forms.Button buttonSubmitPub;
        private System.Windows.Forms.RichTextBox richTextBoxPub;
        private System.Windows.Forms.TabPage tabPageViewPubs;
        private System.Windows.Forms.ListView listView2;
        private System.Windows.Forms.ColumnHeader columnHeader_idpub;
        private System.Windows.Forms.ColumnHeader columnHeader_doi;
        private System.Windows.Forms.ColumnHeader columnHeader_name;
        private System.Windows.Forms.ColumnHeader columnHeader_year;
        private System.Windows.Forms.ColumnHeader columnHeader_affiliations;
        private System.Windows.Forms.ColumnHeader columnHeader_fs_ref;
        private System.Windows.Forms.ColumnHeader columnHeader_Tags;
        private System.Windows.Forms.TextBox textBoxTagsPub;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxAffiliationsPub;
        private System.Windows.Forms.Label labelAffiliationsPub;
        private System.Windows.Forms.TextBox textBoxYearPub;
        private System.Windows.Forms.Label labelYearPub;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.ColumnHeader columnHeader_time_added;
    }
}

